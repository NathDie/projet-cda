<?php

namespace App\Repository;

use App\Entity\DelivererMan;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DelivererMan|null find($id, $lockMode = null, $lockVersion = null)
 * @method DelivererMan|null findOneBy(array $criteria, array $orderBy = null)
 * @method DelivererMan[]    findAll()
 * @method DelivererMan[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DelivererManRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DelivererMan::class);
    }

    public function getByDelivererId($delivererId)
    {
        return $this->createQueryBuilder('r')
        ->andWhere('r.id = :val')
        ->setParameter('val', $delivererId)
        ->getQuery()
        ->getOneOrNullResult()
    ;
    }

    // /**
    //  * @return DelivererMan[] Returns an array of DelivererMan objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DelivererMan
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
