<?php

namespace App\Repository;

use App\Entity\City;
use App\Entity\Sector;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository implements PasswordUpgraderInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function getByDelivererId($delivererId)
    {
        return $this->createQueryBuilder('r')
        ->andWhere('r.deliverer = :val')
        ->setParameter('val', $delivererId)
        ->getQuery()
        ->getOneOrNullResult()
    ;
    }

    public function getByRestaurantId($restaurantId)
    {
        return $this->createQueryBuilder('r')
        ->andWhere('r.restaurant = :val')
        ->setParameter('val', $restaurantId)
        ->getQuery()
        ->getOneOrNullResult()
    ;
    }

    public function getByClientId($clientId)
    {
        return $this->createQueryBuilder('r')
        ->andWhere('r.client = :val')
        ->setParameter('val', $clientId)
        ->getQuery()
        ->getOneOrNullResult()
    ;
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function upgradePassword(UserInterface $user, string $newEncodedPassword): void
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', \get_class($user)));
        }

        $user->setPassword($newEncodedPassword);
        $this->_em->persist($user);
        $this->_em->flush();
    }

     /**
      * @return User[] Returns an array of User objects
      */

    public function findRestaurants(string $sector)
    {
        return $this->createQueryBuilder('u')
            ->innerJoin('u.city' , 'c'  )
            ->innerJoin('c.sector', 's' )
            ->andWhere('u.restaurant IS NOT NULL')
            ->andWhere('s.name = :sector')
            ->setParameter('sector', $sector)
            ->orderBy('u.id', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }
    // pas terminé !!!
    public function findDeliverer(Sector $sector)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.deliverer IS NOT NULL')
            ->orderBy('u.id', 'ASC')
            ->getQuery()
            ->getResult()
            ;
    }
    //pas terminé
    public function findClient(Sector $sector)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.client IS NOT NULL')
            ->orderBy('u.id', 'ASC')
            ->getQuery()
            ->getResult()
            ;
    }


    /*
    public function findOneBySomeField($value): ?User
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
