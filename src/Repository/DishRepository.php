<?php

namespace App\Repository;

use App\Entity\Dish;
use App\Entity\DishType;
use App\Entity\Restaurant;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Dish|null find($id, $lockMode = null, $lockVersion = null)
 * @method Dish|null findOneBy(array $criteria, array $orderBy = null)
 * @method Dish[]    findAll()
 * @method Dish[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DishRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Dish::class);
    }

     /**
      * @return Dish[] Returns an array of Dish objects
      */

    public function findDishTypeByRestaurant(Restaurant $restaurant)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.restaurant = :restaurant')
            ->setParameter('restaurant', $restaurant)
            ->groupBy('d.dishType')
            ->getQuery()
            ->getResult()
        ;
    }
    /**
     * @return Dish[] Returns an array of Dish objects
     */

    public function findDishsByTypeAndRestaurant(Restaurant $restaurant, string $dishType)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.restaurant = :restaurant')
            ->andWhere('d.dishType = :dishType')
            ->setParameter('restaurant', $restaurant)
            ->setParameter('dishType', $dishType)
            ->getQuery()
            ->getResult()
            ;
    }


    /*
    public function findOneBySomeField($value): ?Dish
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
