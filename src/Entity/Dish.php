<?php

namespace App\Entity;

use App\Repository\DishRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DishRepository::class)
 */
class Dish
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $dishDescription;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $dishPrice;

    /**
     * @ORM\OneToMany(targetEntity=OrderDish::class, mappedBy="dish")
     */
    private $orderDishes;

    /**
     * @ORM\ManyToOne(targetEntity=DishType::class, inversedBy="dishes")
     */
    private $dishType;

    /**
     * @ORM\ManyToOne(targetEntity=Restaurant::class, inversedBy="dish")
     * @ORM\JoinColumn(nullable=false)
     */
    private $restaurant;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $stock;

    public function __construct()
    {
        $this->orderDishes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDishDescription(): ?string
    {
        return $this->dishDescription;
    }

    public function setDishDescription(?string $dishDescription): self
    {
        $this->dishDescription = $dishDescription;

        return $this;
    }

    public function getDishPrice(): ?string
    {
        return $this->dishPrice;
    }

    public function setDishPrice(string $dishPrice): self
    {
        $this->dishPrice = $dishPrice;

        return $this;
    }

    /**
     * @return Collection|OrderDish[]
     */
    public function getOrderDishes(): Collection
    {
        return $this->orderDishes;
    }

    public function addOrderDish(OrderDish $orderDish): self
    {
        if (!$this->orderDishes->contains($orderDish)) {
            $this->orderDishes[] = $orderDish;
            $orderDish->setDish($this);
        }

        return $this;
    }

    public function removeOrderDish(OrderDish $orderDish): self
    {
        if ($this->orderDishes->removeElement($orderDish)) {
            // set the owning side to null (unless already changed)
            if ($orderDish->getDish() === $this) {
                $orderDish->setDish(null);
            }
        }

        return $this;
    }

    public function getDishType(): ?DishType
    {
        return $this->dishType;
    }

    public function setDishType(?DishType $dishType): self
    {
        $this->dishType = $dishType;

        return $this;
    }

    public function getRestaurant(): ?Restaurant
    {
        return $this->restaurant;
    }

    public function setRestaurant(?Restaurant $restaurant): self
    {
        $this->restaurant = $restaurant;

        return $this;
    }

    public function getStock(): ?int
    {
        return $this->stock;
    }

    public function setStock(?int $stock): self
    {
        $this->stock = $stock;

        return $this;
    }
}
