<?php

namespace App\Entity;

use App\Repository\RestaurantRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RestaurantRepository::class)
 */
class Restaurant
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $timeTable;

    /**
     * @ORM\OneToMany(targetEntity=ThemeRestaurant::class, mappedBy="restaurant")
     */
    private $themeRestaurants;

    /**
     * @ORM\OneToOne(targetEntity=User::class, mappedBy="restaurant", cascade={"persist", "remove"})
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity=Dish::class, mappedBy="restaurant", orphanRemoval=true)
     */
    private $dish;

    /**
     * @ORM\OneToMany(targetEntity=Order::class, mappedBy="restaurant")
     */
    private $orders;

    public function __construct()
    {
        $this->themeRestaurants = new ArrayCollection();
        $this->dish = new ArrayCollection();
        $this->orders = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTimeTable(): ?\DateTimeInterface
    {
        return $this->timeTable;
    }

    public function setTimeTable(\DateTimeInterface $timeTable): self
    {
        $this->timeTable = $timeTable;

        return $this;
    }

    /**
     * @return Collection|ThemeRestaurant[]
     */
    public function getThemeRestaurants(): Collection
    {
        return $this->themeRestaurants;
    }

    public function addThemeRestaurant(ThemeRestaurant $themeRestaurant): self
    {
        if (!$this->themeRestaurants->contains($themeRestaurant)) {
            $this->themeRestaurants[] = $themeRestaurant;
            $themeRestaurant->setRestaurant($this);
        }

        return $this;
    }

    public function removeThemeRestaurant(ThemeRestaurant $themeRestaurant): self
    {
        if ($this->themeRestaurants->removeElement($themeRestaurant)) {
            // set the owning side to null (unless already changed)
            if ($themeRestaurant->getRestaurant() === $this) {
                $themeRestaurant->setRestaurant(null);
            }
        }

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        // unset the owning side of the relation if necessary
        if ($user === null && $this->user !== null) {
            $this->user->setRestaurant(null);
        }

        // set the owning side of the relation if necessary
        if ($user !== null && $user->getRestaurant() !== $this) {
            $user->setRestaurant($this);
        }

        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|Dish[]
     */
    public function getDish(): Collection
    {
        return $this->dish;
    }

    public function addDish(Dish $dish): self
    {
        if (!$this->dish->contains($dish)) {
            $this->dish[] = $dish;
            $dish->setRestaurant($this);
        }

        return $this;
    }

    public function removeDish(Dish $dish): self
    {
        if ($this->dish->removeElement($dish)) {
            // set the owning side to null (unless already changed)
            if ($dish->getRestaurant() === $this) {
                $dish->setRestaurant(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Order[]
     */
    public function getOrders(): Collection
    {
        return $this->orders;
    }

    public function addOrder(Order $order): self
    {
        if (!$this->orders->contains($order)) {
            $this->orders[] = $order;
            $order->setRestaurant($this);
        }

        return $this;
    }

    public function removeOrder(Order $order): self
    {
        if ($this->orders->removeElement($order)) {
            // set the owning side to null (unless already changed)
            if ($order->getRestaurant() === $this) {
                $order->setRestaurant(null);
            }
        }

        return $this;
    }
}
