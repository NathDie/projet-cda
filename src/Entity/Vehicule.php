<?php

namespace App\Entity;

use App\Repository\VehiculeRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=VehiculeRepository::class)
 */
class Vehicule
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity=DelivererMan::class, inversedBy="vehicule")
     */
    private $delivererMan;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getDelivererMan(): ?DelivererMan
    {
        return $this->delivererMan;
    }

    public function setDelivererMan(?DelivererMan $delivererMan): self
    {
        $this->delivererMan = $delivererMan;

        return $this;
    }

    public function __toString()
    {
        return $this->type;
    }
}
