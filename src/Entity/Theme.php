<?php

namespace App\Entity;

use App\Repository\ThemeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ThemeRepository::class)
 */
class Theme
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=ThemeRestaurant::class, mappedBy="theme")
     */
    private $themeRestaurants;

    public function __construct()
    {
        $this->themeRestaurants = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|ThemeRestaurant[]
     */
    public function getThemeRestaurants(): Collection
    {
        return $this->themeRestaurants;
    }

    public function addThemeRestaurant(ThemeRestaurant $themeRestaurant): self
    {
        if (!$this->themeRestaurants->contains($themeRestaurant)) {
            $this->themeRestaurants[] = $themeRestaurant;
            $themeRestaurant->setTheme($this);
        }

        return $this;
    }

    public function removeThemeRestaurant(ThemeRestaurant $themeRestaurant): self
    {
        if ($this->themeRestaurants->removeElement($themeRestaurant)) {
            // set the owning side to null (unless already changed)
            if ($themeRestaurant->getTheme() === $this) {
                $themeRestaurant->setTheme(null);
            }
        }

        return $this;
    }
}
