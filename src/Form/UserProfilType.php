<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use App\Entity\City;

class UserProfilType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', null, [
                'label' => 'Votre adresse adresse'
            ])
            ->add('plainpassword', TextType::class, [
                'label' => 'Entrez votre nouveau mot de passe si vous souhaitez le changer',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Votre nouveau mot de passe'
                ]
            ])
            ->add('name', null, [
                'label' => 'Votre nom'
            ])
            ->add('street', null, [
                'label' => 'Votre rue'
            ])
            ->add('numStreet', null, [
                'label' => 'Votre numéro de maison'
            ])
            ->add('phoneNumber', null, [
                'label' => 'Votre numéro de téléphone'
            ])
            ->add('city',EntityType::class, [
                'label' => 'Votre ville',
                'class' => City::class,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
