<?php

namespace App\Form;

use App\Entity\DelivererMan;
use App\Entity\DishType;
use App\Entity\Vehicule;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Validator\Constraints\Choice;

class DelivererProfilType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', TextType::class, [
                'label' => 'Votre prénom'
            ]);
            //  ->add('vehiculeType',EntityType::class,[
            //      'class' => Vehicule::class,
            // ])
           // ->add('vehicule', EntityType::class, [
           //     'class' => Vehicule::class,

           // ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => DelivererMan::class,
        ]);
    }
}
