<?php

namespace App\Controller;

use App\Entity\Client;
use App\Entity\DelivererMan;
use App\Entity\Restaurant;
use App\Form\ClientProfilType;
use App\Form\DelivererProfilType;
use App\Repository\ClientRepository;
use App\Repository\DelivererManRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\CartService;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\User\UserInterface;


class HomeController extends AbstractController
{
    /**
     * @Route("/home", name="home")
     */
    public function index():Response
    {
        return $this->render('index.html.twig');
    }


}

