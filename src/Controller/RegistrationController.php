<?php

namespace App\Controller;

use App\Entity\Client;
use App\Entity\DelivererMan;
use App\Entity\Restaurant;
use App\Entity\User;
use App\Entity\Vehicule;
use App\Form\UserProfilType;
use App\Form\ClientProfilType;
use App\Form\DelivererProfilType;
use App\Form\RegistrationFormType;
use App\Form\RestaurantProfilType;
use App\Repository\ClientRepository;
use App\Repository\DelivererManRepository;
use App\Repository\RestaurantRepository;
use App\Repository\UserRepository;
use App\Repository\VehiculeRepository;
use App\Security\AppCustomAuthenticator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;


class RegistrationController extends AbstractController
{
    /**
     * @Route("/register", name="app_register")
     */
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder, GuardAuthenticatorHandler $guardHandler, AppCustomAuthenticator $authenticator): Response
    {
        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // encode the plain password
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();
            // do anything else you need here, like send an email

            return $guardHandler->authenticateUserAndHandleSuccess(
                $user,
                $request,
                $authenticator,
                'main' // firewall name in security.yaml
            );
        }

        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("/register/role", name="register_role")
     */
    public function registerRoles(ClientRepository $clientRepository,VehiculeRepository $vehiculeRepository, DelivererManRepository $delivererManRepository, UserRepository $userRepository, UserInterface $user, Request $request): Response
    {


        $client = new Client();
        $formClient = $this->createForm(ClientProfilType::class, $client);
        $formClient->handleRequest($request);


        if ($formClient->isSubmitted() && $formClient->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $user->setRoles(['ROLE_CLIENT']);
            $user->setClient($client);
            $entityManager->persist($client);
            $entityManager->flush();

            return $this->redirectToRoute('home');
//            return $this->redirectToRoute('gestionClient_index');
        }

      $vehicules = $vehiculeRepository->findAll();


        $restaurant = new Restaurant();
        $formRestaurant = $this->createForm(RestaurantProfilType::class, $restaurant);
        $formRestaurant->handleRequest($request);
        if ($formRestaurant->isSubmitted() && $formRestaurant->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $user->setRoles(['ROLE_RESTAURANT']);
            $user->setRestaurant($restaurant);
            $entityManager->persist($restaurant);
            $entityManager->flush();
            return $this->redirectToRoute('home');
        }


        return $this->render('registration/registertype.html.twig', [
            'formClient' => $formClient->createView(),
            'vehicules'=>$vehicules,
            'formRestaurant'=>$formRestaurant->createView(),
        ]);

    }
    /**
     * @Route("/register/role/deliverer/add", name="register_addRoleDeliverer")
     */
    public function addRoleDeliverer( Request $request, EntityManagerInterface $manager, VehiculeRepository $vehiculeRepository): Response
    {
        if ($request->getMethod('post')){
           $user = $this->getUser();
           $vehicule = new Vehicule();
           $vehicule->setType($request->get('filtreVehicule'));

           $manager->persist($vehicule);
           $manager->flush();
           $vehicule = $vehiculeRepository->findLastVehicule();

           $deliverer = new DelivererMan();
           $deliverer->setFirstName($request->get('prenom'));
             $deliverer->setUser($user);
            $user->setRoles(['ROLE_DELIVERER']);

            $manager->persist($deliverer);
            $manager->flush();

        }

        return $this->redirectToRoute('home');
    }
}

