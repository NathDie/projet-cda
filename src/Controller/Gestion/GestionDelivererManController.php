<?php

namespace App\Controller\Gestion;

use App\Entity\DelivererMan;
use App\Entity\Order;
use App\Entity\User;
use App\Repository\OrderRepository;
use App\Repository\UserRepository;
use ContainerIUhu6xJ\getCityRepositoryService;
use ContainerIUhu6xJ\getOrderRepositoryService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class GestionDelivererManController extends AbstractController
{
    /**
     * @Route("/gestion/delivererman", name="gestion_delivererman")
     */
    public function index(OrderRepository $orderRepository, UserRepository $userRepository): Response
    {
        $user = $userRepository->findOneBy(['email' => $this->getUser()->getUsername()]);
        $sectorName = $user->getCity()->getSector()->getName();
        $orders = $orderRepository->findBy(array('orderStatus' => '4'));

        return $this->render('gestion_deliverer_man/index.html.twig', [
            'controller_name' => 'GestionDelivererManController',
            'sector' => $sectorName,
            'orders' => $orders,
        ]);
    }
    /**
     * @Route("/gestion/delivererman/changestatusaccepeted/{id}", name="gestion_delivererman_changestatusaccepeted")
     */
    public function  changeOrderStatusAccepted(Order $order,OrderRepository $orderRepository){
        $orderRepository->updatedStatutOrder($order->getId(),'5');
        return $this->redirectToRoute('gestion_deliverydetail', [
            'id' => $order->getId()
        ]);
    }

    /**
     * @Route("/gestion/delivererman/changestatusdelivered/{id}", name="gestion_delivererman_changestatusdelivered")
     */
    public function  changeOrderStatusdelivered(Order $order,OrderRepository $orderRepository){
        $orderRepository->updatedStatutOrder($order->getId(),'6');
        return $this->redirectToRoute('gestion_delivererman', [
            'id' => $order->getId()
        ]);
    }



    /**
     * @Route("/gestion/deliverydetail/{id}", name="gestion_deliverydetail")
     */
    public function deliverydetail(Order $order): Response
    {

        return $this->render('gestion_deliverer_man/deliverydetail.html.twig', [
            'order' => $order,
        ]);
    }

    /**
     * @Route("/gestion/deliveryhistory/{id}", name="gestion_deliveryhistory")
     */
    public function deliveryhistory(OrderRepository $orderRepository,DelivererMan $delivererMan): Response
    {
        $orders = $orderRepository->findBy(array('orderStatus' => '6'));

        return $this->render('gestion_deliverer_man/deliveryhistory.html.twig', [
            'orders' => $orders,
            'delivererman'=> $delivererMan,
        ]);
    }


}
