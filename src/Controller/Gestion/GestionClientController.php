<?php

namespace App\Controller\Gestion;


use App\Entity\Dish;
use App\Entity\Order;
use App\Entity\OrderDish;
use App\Entity\Restaurant;
use App\Repository\DishRepository;
use App\Repository\DishTypeRepository;
use App\Repository\OrderRepository;
use App\Repository\OrderStatusRepository;
use App\Repository\ThemeRestaurantRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use mysql_xdevapi\Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class GestionClientController extends AbstractController
{
    /**
     * @Route("/gestion/client", name="gestionClient_index")
     */
    public function index(UserRepository $userRepository,
                          ThemeRestaurantRepository $themeRestaurantRepository,
                          OrderRepository $orderRepository
    ): Response
    {
        $user = $this->getUser();
        $idClient = $user->getClient();
        $sectorName = $user->getCity()->getSector()->getName();
        $restaurants = $userRepository->findRestaurants($sectorName);
        $themes = array();

        foreach ($restaurants as $restaurant) {
            $themeRestaurants = $themeRestaurantRepository->findOneBy(['restaurant' => $restaurant->getRestaurant()->getId()]);
            $idRestaurant = $themeRestaurants->getRestaurant()->getId();
            $themes[$idRestaurant] = $themeRestaurants->getTheme()->getName();
        }
        $ordersNotFinished = $orderRepository->findBy(['client' => $idClient]);

        return $this->render('gestion_client/index.html.twig', [
            'controller_name' => 'GestionClientController',
            'restaurants' => $restaurants,
            'themes' => $themes,
            'orders' => $ordersNotFinished
        ]);
    }

    /**
     * @Route("/gestion/client/restaurant/{restaurant}", name="gestionClient_restaurant")
     */
    public function showRestaurant(Restaurant $restaurant, DishRepository $dishRepository,
                                   SessionInterface $session,
                                   Request $request
    ): Response
    {
        $dishs = $dishRepository->findBy(['restaurant' => $restaurant]);

        //filtration
        $restaurantDishs = $dishRepository->findDishTypeByRestaurant($restaurant);


        if ($session->get('restaurant')) {
//            dd($session->get('restaurant'), $restaurant->getId());
            if ($session->get('restaurant') != $restaurant->getId()) {
                $session->remove('panier');
                $session->remove('totalPrice');
                $session->remove('restaurant');
                $session->remove('notEnoughtStock');
            }
        }
        if ($request->getMethod() == "POST" && $request->get('filtreDishType') != 0) {
            $idDisthType = $request->get('filtreDishType');
            $dishs = $dishRepository->findDishsByTypeAndRestaurant($restaurant, $idDisthType);
        }

        $panier = $session->get('panier', []);
        $panierWithData = [];
        $totalPrice = 3.0;

        foreach ($panier as $id => $quantity) {
            $dish = $dishRepository->find(['id' => $id]);
            $panierWithData[] = [
                "dish" => $dish,
                "quantity" => $quantity
            ];
            $totalPrice += $dish->getDishPrice() * $quantity;
        }
        $session->set('totalPrice', $totalPrice);
        $session->set('restaurant', $restaurant->getId());


        return $this->render('gestion_client/restaurant.html.twig', [
            'restaurant' => $restaurant,
            'dishs' => $dishs,
            'paniers' => $panierWithData,
            'totalPrice' => $totalPrice,
            'restaurantDishs' => $restaurantDishs
        ]);
    }

    /**
     * @Route("/gestion/client/panier/add/{dish}", name="gestionClient_AddCart")
     */
    public function addDishToCart(Dish $dish, SessionInterface $session): Response
    {
        $panier = $session->get('panier', []);
        if (!empty($panier[$dish->getId()])) {
            if ($panier[$dish->getId()] < $dish->getStock()) {
                $panier[$dish->getId()]++;
                $session->remove('notEnoughtStock');
            } else {
                $session->set('notEnoughtStock', true);

            }
        } else {
            $panier[$dish->getId()] = 1;
            $session->remove('notEnoughtStock');
        }

        $restaurantId = $dish->getRestaurant()->getId();

        $session->set('panier', $panier);


        return $this->redirectToRoute('gestionClient_restaurant', [
            'restaurant' => $restaurantId,
        ]);
    }

    /**
     * @Route("/gestion/client/panier/remove/{dish}", name="gestionClient_removeCart")
     */
    public function removeDishToCart(Dish $dish, SessionInterface $session): Response
    {
        $panier = $session->get('panier');
        $session->remove('notEnoughtStock');
        $panier[$dish->getId()]--;

        if ($panier[$dish->getId()] == 0) {
            unset($panier[$dish->getId()]);
        }

        $restaurantId = $dish->getRestaurant()->getId();

        $session->set('panier', $panier);

        return $this->redirectToRoute('gestionClient_restaurant', [
            'restaurant' => $restaurantId
        ]);
    }

    /**
     * @Route("/gestion/client/order/add", name="gestionClient_newOrder")
     */
    public function addNewClientOrder(SessionInterface $session, DishRepository $dishRepository,
                                      OrderStatusRepository $orderStatusRepository, Request $request,
                                      EntityManagerInterface $manager
    ): Response
    {
        if (!$session->get('panier')){
            $session->set('cartEmpty', true);
            return $this->redirectToRoute('gestionClient_index');
        }


        $status = $orderStatusRepository->findOneBy(['id' => 1]);
        $idDish = key($session->get('panier'));
        $totalPrice = $session->get('totalPrice');
        $dish = $dishRepository->findOneBy(['id' => $idDish]);
        $restaurant = $dish->getRestaurant();


        if ($request->getMethod() == "POST") {
            try {
                $delivererDate = \DateTime::createFromFormat("Y-m-d\TH:i", $request->get("delivererDate"));
            } catch (Exception $e) {
                echo $e->getMessage();
            }
            $order = new Order();
            $order->setClient($this->getUser()->getClient())
                ->setDeliveryDate($delivererDate)
                ->setDeliveryPrice(3.00)
                ->setOrderDate(new \DateTime())
                ->setPriceTTC($totalPrice)
                ->setPriceHT($totalPrice * 0.945)
                ->setRestaurant($restaurant)
                ->setOrderStatus($status);
            $manager->persist($order);

            //inserer les details plats
            // plat avec la quantité
            $panier = $session->get('panier');
            foreach ($panier as $id => $quantity) {
                $orderDish = new OrderDish();
                $orderDish->setDish($dishRepository->find(['id' => $id]));
                $orderDish->setQuantity($quantity);
                $orderDish->setOrders($order);
                $manager->persist($orderDish);
            }

            //gestion stock
            // dish avec qté

            foreach ($panier as $id => $quantity) {
                $dish = $dishRepository->find(['id' => $id]);
                $actualStock = $dish->getStock();
                $newStock = $actualStock - $quantity;
                $dish->setStock($newStock);
            }

            $manager->flush();

            $session->remove('panier');
            $session->remove('totalPrice');
            $session->remove('restaurant');
            $session->remove('notEnoughtStock');
            $session->remove('cartEmpty');
        }

        return $this->redirectToRoute('gestionClient_index', [
        ]);
    }
}
