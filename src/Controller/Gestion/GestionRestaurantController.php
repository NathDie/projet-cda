<?php

namespace App\Controller\Gestion;

use App\Entity\Dish;
use App\Entity\Order;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\RestaurantRepository;
use App\Repository\DishRepository;
use App\Form\OrderStatutType;
use App\Form\DishManageType;
use Doctrine\ORM\EntityManagerInterface;

class GestionRestaurantController extends AbstractController
{
    /**
     * @var DishRepository
     */
    private $repository;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em, DishRepository $repository)
    {
        $this->em = $em;
        $this->repository = $repository;
    }

    /**
     * @Route("/gestion/restaurant", name="gestion_restaurant_index")
     */
    public function index(RestaurantRepository $restaurantRepository): Response
    {
        $restaurantId = $this->getUser()->getRestaurant()->getId();

        $restaurant = $restaurantRepository->getByRestaurantId($restaurantId);

        return $this->render('gestion_restaurant/index.html.twig', [
            'controller_name' => 'GestionRestaurantController',
            'restaurant' => $restaurant
        ]);
    }

    /**
     * @Route("/gestion/restaurant/plats", name="gestion_restaurant_plat_list")
     */
    public function indexPlats()
    {
        $dishs = $this->repository->findAll();
        return $this->render('gestion_restaurant/listDish.html.twig', [
            'controller_name' => 'AdminDishController',
            'dishs' => $dishs
        ]);
    }

    /**
     * @Route("/gestion/restaurant/plat/add", name="gestion_restaurant_plat_add")
     */
    public function newDish(Request $request, RestaurantRepository $restaurantRepository)
    {
        $dish = new Dish();
        $form = $this->createForm(DishManageType::class, $dish);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $restaurantId = $this->getUser()->getRestaurant()->getId();

            $restaurant = $restaurantRepository->getByRestaurantId($restaurantId);
            $dish->setRestaurant($restaurant);
            $this->em->persist($dish);
            $this->em->flush();
            $this->addFlash('add', 'Votre plat a été crée');
            return $this->redirectToRoute('gestion_restaurant_plat_list');
        }
        return $this->render('gestion_restaurant/manageDish.html.twig', [
            'controller_name' => 'GestionRestaurantController',
            'dish' => $dish,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/gestion/restaurant/plat/{id}", name="gestion_restaurant_plat_edit")
     */
    public function editDish(Dish $dish, Request $request)
    {
        $form = $this->createForm(DishManageType::class, $dish);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();
            $this->addFlash('edit', 'Votre plat a été modifié');
            return $this->redirectToRoute('gestion_restaurant_plat_list');
        }
        return $this->render('gestion_restaurant/manageDish.html.twig', [
            'controller_name' => 'AdminUserController',
            'dish' => $dish,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/gestion/restaurant/plat/{id}/delete", name="gestion_restaurant_plat_delete")
     */
    public function deleteDish(Dish $dish, Request $request)
    {
        if ($this->isCsrfTokenValid('delete' . $dish->getId(), $request->get('_token'))) {
            $this->em->remove($dish);
            $this->em->flush();
            $this->addFlash('supp', 'Votre plat a été supprimé');
            return $this->redirectToRoute('gestion_restaurant_plat_list');
        } else {
            $this->addFlash('error', 'Le jeton CSRF est invalide. Veuillez renvoyer le formulaire');
            return $this->redirectToRoute('gestion_restaurant_plat_list');
        }
    }

    /**
     * @Route("/gestion/restaurant/commande/{id}", name="gestion_restaurant_show_and_statut_commande")
     */
    public function ShowOrderAndEditOrderStatut(Order $order, Request $request)
    {
        $form = $this->createForm(OrderStatutType::class, $order);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();
            $this->addFlash('edit', 'La commande a été modifié');
            return $this->redirectToRoute('gestion_restaurant_index');
        }

        return $this->render('gestion_restaurant/show.html.twig', [
            'controller_name' => 'GestionRestaurantController',
            'order' => $order,
            'form' => $form->createView()
        ]);
    }
}
