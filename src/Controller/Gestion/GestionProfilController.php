<?php

namespace App\Controller\Gestion;

use App\Repository\DelivererManRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Form\UserProfilType;
use App\Form\DelivererProfilType;
use App\Form\RestaurantProfilType;
use App\Form\ClientProfilType;
use App\Repository\ClientRepository;
use App\Repository\RestaurantRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class GestionProfilController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;


    public function __construct(EntityManagerInterface $em, UserPasswordEncoderInterface $encoder)
    {
        $this->em = $em;
        $this->encoder = $encoder;
    }

    /**
     * @Route("/gestion/delivererman/profil", name="gestion_delivererman_profil")
     */
    public function indexDelivererProfil(DelivererManRepository $delivererManRepository, UserRepository $userRepository, Request $request): Response
    {
        $delivererId = $this->getUser()->getDeliverer()->getId();

        $deliverer = $delivererManRepository->getByDelivererId($delivererId);
        $delivererUser = $userRepository->getByDelivererId($delivererId);

        $form = $this->createForm(DelivererProfilType::class, $deliverer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();
            $this->addFlash('editProfil', 'Vos informations ont été modifié');
            return $this->redirectToRoute('gestion_delivererman_profil');
        }

        $formUser = $this->createForm(UserProfilType::class, $delivererUser);
        $formUser->handleRequest($request);

        if ($formUser->isSubmitted() && $formUser->isValid()) {
            if ($delivererUser->getPlainPassword()) {
                $pwd = $this->encoder->encodePassword($delivererUser, $delivererUser->getPlainPassword());
                $delivererUser->setPassword($pwd);
            }
            $this->em->flush();
            $this->addFlash('editProfil', 'Vos informations ont été modifié');
            return $this->redirectToRoute('gestion_delivererman_profil');
        }

        return $this->render('gestion_profil/livreur_profil.html.twig', [
            'controller_name' => 'GestionProfilController',
            'form' => $form->createView(),
            'formUser' => $formUser->createView(),
            'deliverer' => $deliverer
        ]);
    }

    /**
     * @Route("/gestion/restaurant/profil", name="gestion_restaurant_profil")
     */
    public function indexRestaurantProfil(RestaurantRepository $restaurantRepository, UserRepository $userRepository, Request $request): Response
    {
        $restaurantId = $this->getUser()->getRestaurant()->getId();

        $restaurant = $restaurantRepository->getByRestaurantId($restaurantId);
        $restaurantUser = $userRepository->getByRestaurantId($restaurantId);

        $form = $this->createForm(RestaurantProfilType::class, $restaurant);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();
            $this->addFlash('editProfil', 'Vos informations ont été modifié');
            return $this->redirectToRoute('gestion_restaurant_profil');
        }

        $formUser = $this->createForm(UserProfilType::class, $restaurantUser);
        $formUser->handleRequest($request);

        if ($formUser->isSubmitted() && $formUser->isValid()) {
            if ($restaurantUser->getPlainPassword()) {
                $pwd = $this->encoder->encodePassword($restaurantUser, $restaurantUser->getPlainPassword());
                $restaurantUser->setPassword($pwd);
            }
            $this->em->flush();
            $this->addFlash('editProfil', 'Vos informations ont été modifié');
            return $this->redirectToRoute('gestion_restaurant_profil');
        }

        return $this->render('gestion_profil/restaurant_profil.html.twig', [
            'controller_name' => 'GestionProfilController',
            'form' => $form->createView(),
            'formUser' => $formUser->createView()
        ]);
    }

    /**
     * @Route("/gestion/client/profil", name="gestion_client_profil")
     */
    public function indexClientProfil(ClientRepository $clientRepository, UserRepository $userRepository, Request $request): Response
    {
        $restaurantId = $this->getUser()->getClient()->getId();

        $client = $clientRepository->getByClientId($restaurantId);
        $clientUser = $userRepository->getByClientId($restaurantId);

        $form = $this->createForm(ClientProfilType::class, $client);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();
            $this->addFlash('editProfil', 'Vos informations ont été modifié');
            return $this->redirectToRoute('gestion_client_profil');
        }

        $formUser = $this->createForm(UserProfilType::class, $clientUser);
        $formUser->handleRequest($request);

        if ($formUser->isSubmitted() && $formUser->isValid()) {
            if ($clientUser->getPlainPassword()) {
                $pwd = $this->encoder->encodePassword($clientUser, $clientUser->getPlainPassword());
                $clientUser->setPassword($pwd);
            }
            $this->em->flush();
            $this->addFlash('editProfil', 'Vos informations ont été modifié');
            return $this->redirectToRoute('gestion_client_profil');
        }

        return $this->render('gestion_profil/client_profil.html.twig', [
            'controller_name' => 'GestionProfilController',
            'form' => $form->createView(),
            'formUser' => $formUser->createView(),
            'client' => $client
        ]);
    }
}
